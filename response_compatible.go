package http

import (
	"net/http"

	"crypto/tls"
	utls "gitlab.com/go-extension/tls"
)

func ToResponse(r *http.Response) *Response {
	if r == nil {
		return nil
	}

	var cs *utls.ConnectionState
	if r.TLS != nil {
		ucs := utls.ToConnectionState(*r.TLS)
		cs = &ucs
	}

	return &Response{
		Status:           r.Status,
		StatusCode:       r.StatusCode,
		Proto:            r.Proto,
		ProtoMajor:       r.ProtoMajor,
		ProtoMinor:       r.ProtoMinor,
		Header:           Header(r.Header),
		Body:             r.Body,
		ContentLength:    r.ContentLength,
		TransferEncoding: r.TransferEncoding,
		Close:            r.Close,
		Uncompressed:     r.Uncompressed,
		Trailer:          Header(r.Trailer),
		Request:          ToRequest(r.Request),
		TLS:              cs,
	}
}

func FromResponse(r *Response) *http.Response {
	if r == nil {
		return nil
	}

	var cs *tls.ConnectionState
	if r.TLS != nil {
		ucs := r.TLS.Compatible()
		cs = &ucs
	}

	return &http.Response{
		Status:           r.Status,
		StatusCode:       r.StatusCode,
		Proto:            r.Proto,
		ProtoMajor:       r.ProtoMajor,
		ProtoMinor:       r.ProtoMinor,
		Header:           http.Header(r.Header),
		Body:             r.Body,
		ContentLength:    r.ContentLength,
		TransferEncoding: r.TransferEncoding,
		Close:            r.Close,
		Uncompressed:     r.Uncompressed,
		Trailer:          http.Header(r.Trailer),
		Request:          FromRequest(r.Request),
		TLS:              cs,
	}
}
