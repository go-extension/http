package http

import (
	"net/http"

	"crypto/tls"

	utls "gitlab.com/go-extension/tls"
)

func ToRequest(r *http.Request) *Request {
	if r == nil {
		return nil
	}

	var cs *utls.ConnectionState
	if r.TLS != nil {
		ucs := utls.ToConnectionState(*r.TLS)
		cs = &ucs
	}

	return &Request{
		Method:           r.Method,
		URL:              r.URL,
		Proto:            r.Proto,
		ProtoMajor:       r.ProtoMajor,
		ProtoMinor:       r.ProtoMinor,
		Header:           Header(r.Header),
		Body:             r.Body,
		GetBody:          r.GetBody,
		ContentLength:    r.ContentLength,
		TransferEncoding: r.TransferEncoding,
		Close:            r.Close,
		Host:             r.Host,
		Form:             r.Form,
		PostForm:         r.PostForm,
		MultipartForm:    r.MultipartForm,
		Trailer:          Header(r.Trailer),
		RemoteAddr:       r.RemoteAddr,
		RequestURI:       r.RequestURI,
		TLS:              cs,
		Cancel:           r.Cancel,
		Response:         ToResponse(r.Response),
		ctx:              r.Context(),
	}
}

func FromRequest(r *Request) *http.Request {
	if r == nil {
		return nil
	}

	var cs *tls.ConnectionState
	if r.TLS != nil {
		ucs := r.TLS.Compatible()
		cs = &ucs
	}

	return (&http.Request{
		Method:           r.Method,
		URL:              r.URL,
		Proto:            r.Proto,
		ProtoMajor:       r.ProtoMajor,
		ProtoMinor:       r.ProtoMinor,
		Header:           http.Header(r.Header),
		Body:             r.Body,
		GetBody:          r.GetBody,
		ContentLength:    r.ContentLength,
		TransferEncoding: r.TransferEncoding,
		Close:            r.Close,
		Host:             r.Host,
		Form:             r.Form,
		PostForm:         r.PostForm,
		MultipartForm:    r.MultipartForm,
		Trailer:          http.Header(r.Trailer),
		RemoteAddr:       r.RemoteAddr,
		RequestURI:       r.RequestURI,
		TLS:              cs,
		Cancel:           r.Cancel,
		Response:         FromResponse(r.Response),
	}).WithContext(r.Context())
}
