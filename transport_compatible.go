package http

import "net/http"

type CompatableTransport struct {
	Transport
}

// RoundTrip implements the RoundTripper interface.
//
// For higher-level HTTP client support (such as handling of cookies
// and redirects), see Get, Post, and the Client type.
//
// Like the RoundTripper interface, the error types returned
// by RoundTrip are unspecified.
func (t *CompatableTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	response, err := t.Transport.RoundTrip(ToRequest(req))
	if err != nil {
		return nil, err
	}

	return FromResponse(response), nil
}
