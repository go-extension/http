# HyperText Transfer Protocol (HTTP)

This repository is an extension of the golang standard library [net/http](https://pkg.go.dev/net/http)

# Features

- [Extension TLS](gitlab.com/go-extension/tls)
